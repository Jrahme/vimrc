"=======================
"====== FILE TYPES =====
"=======================
au BufNewFile,BufRead *.yaml set filetype=yaml
au BufNewFile,BufRead *.yml set filetype=yaml
au BufNewFile,BufRead *.rs set filetype=rust
au BufNewFile,BufRead *.clj set filetype=clojure
autocmd BufNewFile,BufReadPost *.wast,*.wat setlocal filetype=wast
"=============================
"====== EDITOR SETTINGS ======
"=============================
set nocompatible "use things vi can't
filetype indent plugin on "required, makes everything nicer
set synmaxcol=319 "to help keep things from slowing down with long horizontal lines
set hidden "stash unwritten files in buffer
set noswapfile "no swap files because they suck
set wildmenu "tabcomplete for some commands
set showcmd "count highlighted
set hlsearch "hightlight search results
set ignorecase "ignore case when searching
set smartcase "but don't ignore case if I use an uppercase letter
set backspace=indent,eol,start "backspace over indents eol and sol 
set nostartofline  "make some movements more intuitive regarding SoL
set confirm "don't quietly fail because of unsaved file, ask first
set cmdheight=2 "Number of lines to use for command line
set autoread "update file when it is changed but not deleted in the filesystem
set notimeout ttimeout ttimeoutlen=200 "timeout for key bindings
"use F7 for formatting
map <F7> mzgg=G`z` 
set nobackup "save on clutterish backup files
set tabstop=2 "indent using a tab size of 2 spaces
set softtabstop=2 "^^^
set shiftwidth=2 "^^^
set expandtab
set autoindent "^^^
set autoread
"set noet "use tabs for indentation becasue tabs > spaces
"autocmd FileType * set noexpandtab "force noexpandtab for all file types
"colorscheme gruvbox "set the colour scheme 
"colorscheme badwolf
colorscheme badwolf
set background=dark "use a dark colour scheme
"set linespace=0 "number of pixels between characters 
set t_Co=256 "Make vim think we have all the colours!
"set modelines=0 "Don't read forst/last lines of file for settings
set cursorline "Be very obvious about what line you are on
set nowrap "Don't wrap long lines
set incsearch "incremental search for performance in larger files
set wrapscan "wrap around the file when searching
let mapleader=" " "set the mapleader character to space (%20)
set cc=150 "horizonal rule at the 150th character
"set list lcs=tab:\▏\ 

"====================================
"====== EDITOR STATUS DISPLAYS ======
"====================================

set ruler "Display cursor position on last line of screen or in status line
set number "show line number
set relativenumber "for any line I'm not on show the line number relative to the cursour
set laststatus=2 "always show status line
set novisualbell "don't be annoying when I fuck up
set title "Change the title of the terminal
set history=1000 "Cap to a decent history size for cmd history

"=============================
"====== SYNTAX SETTINGS ======
"============================

syntax on "syntax highlighting
syntax sync minlines=256 "Life saving for large files and slow machines
set synmaxcol=2048 "same as minlines but for how far out to hightlight lines
set autoindent "keep code pretty and indented
set smartindent "be even better about keeping code pretty

"=========================================
"====== MODIFY TERMINAL ENVIRONMENT ======
"=========================================

if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif

"=================================
"====== CUSTOM KEY BINDINGS ======
"=================================

"--------------------------
"----disable arrow keys----
"--------------------------
noremap  <up> <nop>
inoremap <up> <nop>

noremap <down> <nop>
inoremap <down> <nop>

noremap <left> <nop>
noremap <right> <nop>

inoremap <left> <nop>
inoremap <right> <nop>
" B A start
" Joke shamelessly stolen from this blog post https://tylercipriani.com/blog/2017/06/14/literate-vimrc

"------------------------------------
"---- screen splitting shortcuts ----
"------------------------------------
nmap <Leader><bar> :vsplit<CR>

"=============================
"====== PROFILING ============
"=============================
"A function to enable profiling of plugins on start, will log to
"/tmp/vim-profiling.log
function Prologging()
	let l:profileDir = "/tmp/vim-profiling.log"
	if (!filereadable(l:profileDir))
		silent execute "! touch " . l:profileDir
	endif
	profile start l:profileDir
	profile func *
	profile file *
endfunction

"check for environment variable for if logging should be done or not
if (strlen($PROFILE_VIM) != 0)
	call Prologging()
endif


"=============================
"====== PLUGIN SETTINGS ======
"=============================

"----------------------
"===== Git Gutter =====
"----------------------
let g:gitgutter_diff_ars = '-w'

"------------------------
"====airline settings==== "Lightweight status line enchancements
"------------------------

"let g:airline_theme='gruvbox' "Match the colour scheme
let g:airline_theme='badwolf'
let g:airline_exclude_preview = 1 "Don't show preview in a preview window
let g:bufferline_echo = 0  "Don't echo the bufferline 
let g:airline#extensions#bufferline#enabled = 0 "Disable settings for use with bufferline
let g:airline#extensions#branch#enabled = 1 "Enable git integration with vim fugitive
let g:airline_detect_spell = 0 "Don't show if in spell mode or not
let g:airline_powerline_fonts = 1 "Use the power line fonts (may need to patch fonts)

"---------------------------
"===Vim Clojure Highlight=== "A plugin to further highlight clj code
"---------------------------
autocmd BufRead *.clj try | silent! Require | catch /^Fireplace/ | endtry

"------------------
"======TagBar======
"------------------
"open / close the tag bar for navigation
"Use this binding if you prefer the defautl behaviour of CTRL-T
"nmap <Leader><t> :TagbarToggle<CR>
nmap <C-T> :TagbarToggle<CR>
"show line number and relative line number in tagbar window
let g:tagbar_show_linenumbers = 3

"-----------------
"======Snipe======
"-----------------
"More accurate forward / backward jumping with f/F
map f <Plug>(snipe-f)
map F <plug>(snipe-F)

"---------------
"=====CtrlP=====
"---------------
let g:ctrlp_working_path_mode = 'ra'

"--------------------------
"=====Vim-Docker-Tools=====
"--------------------------
let g:dockertools_docker_host = 'unix:///var/run/docker.sock'
let g:dockertools_user_key_mapping = {'ui-filter' : 'W'}

"====================
"=====IndentLine=====
"====================
let g:indentLine_char = '┆'
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_first_char = '┆'
let g:indentLine_color_term = '6'
let g:indentLine_bgcolor_term = 'NONE'

"============================
"=====Language Functions=====
"============================

"------------------------
"=====Edit With Tabs=====
"------------------------
"For passed in filetype
"Open file type and conver tabs to spaces
"Before saving convert tabs back to spaces
"After saving convert spaces back to tabs

function! EditWithTabs(ft)
	execute "au BufRead " . a:ft . " %retab!"
	execute "au BufWritePre " . a:ft . " set expandtab" 
	execute "au BufWritePre " . a:ft . " %retab!"
	execute "au BufWritePost " . a:ft . " set noexpandtab"
	execute "au BufWritePost " . a:ft . " %retab!"
endfunction


























































































"=====================================
"===== END OF VIMRC REQUIREMENTS =====
"=====================================
hi NonText ctermbg=none 
hi Normal guibg=NONE ctermbg=NONE
"packloadall
silent! helptags ALL

