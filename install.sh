#!/bin/bash

## Install Vim
if [ $(which vim) ]; then
	sudo apt remove vim -y
fi

CD=$PWD

cd /tmp
git clone https://github.com/vim/vim.git
cd vim

sudo apt install build-essential libncurses5-dev libncursesw5-dev ncurses-base ncurses-bin ncurses-term -y
make distclean
./configure --enable-multibyte --with-features=huge --enable-rubyinterp=yes --enable-pythoninterp=yes --enable-python3interp=yes --enable-perlinterp=yes --enable-luainterp=yes --enable-cscope=yes --prefix=/usr --with-x
make
sudo make install
cd $CD
## copy config
ln -s  $PWD/vimrc /home/$USER/.vimrc
